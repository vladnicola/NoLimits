#!/bin/bash

Enabled=$(xinput list-props 18 | head -2 | tail -1 | cut -c 24)

if [ "$Enabled" == "1" ]; then
	xinput --disable 18
	paplay /usr/share/sounds/freedesktop/stereo/message.oga
else
	xinput --enable 18
	paplay /usr/share/sounds/freedesktop/stereo/message.oga
fi
