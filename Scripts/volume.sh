#!/bin/bash

VOLUME=$1

for SINK in `pacmd list-sinks | grep 'index:' | cut -b12-`
do
	if [ "$VOLUME" == "mute" ]; then
		paplay /usr/share/sounds/freedesktop/stereo/message.oga
		pactl set-sink-mute $SINK toggle
		paplay /usr/share/sounds/freedesktop/stereo/message.oga
	else
		pactl set-sink-volume $SINK $VOLUME
		paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
	fi
done
