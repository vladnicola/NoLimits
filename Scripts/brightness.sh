#!/bin/bash

Brightness=$(cat /sys/class/backlight/intel_backlight/brightness)
MaxBrightness=$(cat /sys/class/backlight/intel_backlight/max_brightness)

Brightness=$((Brightness + $1))

if [ $Brightness -gt $MaxBrightness ]; then
	Brightness=$MaxBrightness
fi

if [ 10 -gt $Brightness ]; then
	Brightness=10
fi

echo $Brightness > /sys/class/backlight/intel_backlight/brightness
