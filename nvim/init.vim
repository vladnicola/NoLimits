call plug#begin()
Plug 'arcticicestudio/nord-vim'
Plug 'dracula/vim',{'as':'dracula'}
call plug#end()

" colorscheme
syntax on
let g:dracula_colorterm = 0
colorscheme nord

" indentation
set noautoindent
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4

" set numbers
set nu
set rnu

" bottom bar details
set laststatus=2

" incremental search
set incsearch

" enable syntax and plugins (for netrw)
syntax enable
filetype plugin on

"""""" FUZZY FILE FINDING

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" Now we can
" - Hit tab to :find by partial match
" - Use * to make it fuzzy

"Things to consider:
" - :b lets you autocomplete any open buffer
""""""

"""""" TAG JUMPING
" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R .

" Now we can:
" - Use ^] to jump to tag under cursor
" - User g^] for ambiguous tags
" - Use ^t to jump back up the tag stack

" Things to consider:
" - This doesn't help if you want a visual list of tags
""""""
